﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpiritOmniProAPI.Models
{
    public class ValuesRequest
    {
        public string organisationid { get; set; }
        public string remaininghours { get; set; }
        public string lasttimespent { get; set; }
    }
}
