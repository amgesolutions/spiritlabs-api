﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SpiritOmniProAPI.Models;

namespace SpiritOmniProAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IHttpClientFactory httpClientFactory;
        
        public ValuesController(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
           
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id) => $"value {id}";

        // POST api/values
        [HttpPost]
        public async Task PostAsync([FromBody] ValuesRequest value)
        {
            int orgId = Convert.ToInt32(value.organisationid);
            int remainHrs = Convert.ToInt32(value.remaininghours);
            int lastTime = Convert.ToInt32(value.lasttimespent);

            int newremainhrs = remainHrs - (lastTime / 60);
            var data = new
            {
                organization = new
                {
                    organization_fields = new
                    {
                        remaining_support_mins = newremainhrs
                    }
                }
            };

            var json = JsonConvert.SerializeObject(data);
            var stringContent = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var client = httpClientFactory.CreateClient("Organizations");
            
            var response = await client.PutAsync($"{orgId}.json", stringContent);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
